const functions = require("firebase-functions");
const express = require("express");

const cors = require("cors");
const stripe = require("stripe")("sk_test_51Jhcv2SAcSM7jFoUgwoD6GppBV0zvomVQJ5RKzpdkpQIOHmiaM5vruP88VM6xf8U3Zv1AExkbmsbSw9GwyCs46ic00ovYjljWW");

//API

//App Config
const app = express();

//Middle ware
app.use(cors({ origin:true }));
app.use(express.json());

//API routes
app.get('/', (request, response) => response.status(200).send('hello world'));

app.post('/payments/create', async (request, response) => {
    const total = request.query.total;

    console.log('payment request received boom!!! for this amount>>>', total);

    const paymentIntent = await stripe.paymentIntents.create({
        amount: total,
        currency: "inr",
    });

    response.status(201).send({
        clientSecret: paymentIntent.client_secret,
    });
})

//Listen command
exports.api = functions.https.onRequest(app);

//http://localhost:5001/clone-f5994/us-central1/api