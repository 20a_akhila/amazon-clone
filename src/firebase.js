/*import {initializeApp} from 'firebase/app';
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import getFireStore from 'firebase/firestore/lite';*/
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';


// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCx8fLsVn6r9Bc-W4-G7By5h7CQJ6TFXpo",
  authDomain: "clone-f5994.firebaseapp.com",
  databaseURL: "https://clone-f5994.firebaseio.com/",
  projectId: "clone-f5994",
  storageBucket: "clone-f5994.appspot.com",
  messagingSenderId: "197433179311",
  appId: "1:197433179311:web:fbaac1747dd6657dd8e85a",
  measurementId: "G-SY44TJBQZ8"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();

export { db, auth };