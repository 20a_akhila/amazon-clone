import React from 'react';
import "./Checkout.css";
//Components
import Subtotal from '../Subtotal';
import { useStateValue } from '../../StateProvider';
import CheckoutProduct from '../CheckoutProduct';
import FlipMove from 'react-flip-move';

function Checkout() {
    const [{basket, user}, dispatch] = useStateValue();

    return (
        <div className="checkout">
            <div className="checkout__left">
                <img 
                    className="checkout__ad" 
                    src="https://images-eu.ssl-images-amazon.com/images/G/31/home_private_label/bhardwl/media_teaser/xcm_banners_tv_pc_1500x150_in-en.jpg" 
                    alt="Advertisment"
                />

                <div>
                    <h3>{user ? 'Hello, ' : null}{user ? user.email : null}</h3>
                    <h2 className="checkout__title">
                        Your shopping Basket
                    </h2>
                    
                    {basket.map(item => (
                        <CheckoutProduct
                            key={item.id}
                            id={item.id}
                            title={item.title}
                            price={item.price}
                            image={item.image}
                            rating={item.rating}
                        />
                    ))}
                    
                </div>
            </div>

            <div className="checkout__right">
                <Subtotal />
            </div>           
        </div>
    )
}

export default Checkout;
