import React, { useState } from 'react';
import './MiniCartItems.css';
import { useStateValue } from '../../StateProvider';

function MiniCartItems({id, title}) {
    const [{basket}, dispatch] = useStateValue();

    const [hide, setHide] = useState(false);

    const removeFromBasket = () => {
        this.deleteRow.bind(this, id);
    }

    return (
        
        <div className="minicartItems">
            <div className="minicartItems__info">
                <p className="minicartItems__title">{title}</p>
                <p className="minicartItems__message">Added to Cart</p>
            </div>
        </div>
    )
}

export default MiniCartItems;
