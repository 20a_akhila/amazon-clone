import React from 'react';
//Styles
import "./Home.css";
//Components
import Product from '../Product';
import SimpleImageSlider from "react-simple-image-slider";
import { useStateValue } from '../../StateProvider';
import MiniCartItems from '../MiniCartItems';
import { Card, ListGroup } from 'react-bootstrap';

function Home() {
    const [{basket}, dispatch] = useStateValue();
    const handleDragStart = (e) => e.preventDefault();

    const items = [
        {url: "https://m.media-amazon.com/images/I/71FSVYGiPEL._SX3000_.jpg"},
        {url: "https://m.media-amazon.com/images/I/71rKhX5FN1L._SX3000_.jpg"},
        {url: "https://m.media-amazon.com/images/I/712NesXeK5L._SX3000_.jpg"},
    ];
    return (
        <div className="home">
            <div className="home__container">    
                <div className="home__image">
                    <SimpleImageSlider width={1500} height={634} slideDuration={2.0} images={items} showNavs={true} />
                </div>
                <div className="home__row">
                    <Product
                        id='001'
                        title="The Lean Startup: How Constant Innovation Creates Radically Successful Businesses"
                        price={29.99}
                        image="https://m.media-amazon.com/images/I/81RCff1NpnL._AC_UY436_FMwebp_QL65_.jpg"
                        rating={5}
                    />
                    <Product
                        id='625387'
                        title="Reach AB-110 Air Bike Exercise Fitness Cycle with Moving or Stationary Handle Adjustments for Home"
                        price={103.81}
                        image="https://m.media-amazon.com/images/I/51-3qDmfc2L._SX679_.jpg"
                        rating={5}
                    />
                </div>

                <div className="home__row">
                    <Product
                        id='00000002'
                        title="TASLAR Stainless Steel Band Strap Wristband with Buckle Adjuster for Samsung Galaxy Watch 3 45mm/Galaxy Watch 46mm/Samsung Gear S3 Smartwatch, Metal, Black"
                        price={30.00}
                        image="https://m.media-amazon.com/images/I/61UR3gVGCHL._SX679_.jpg"
                        rating={4}
                    />
                    <Product
                        id='63829462'
                        title="Echo Dot (4th Gen, 2020 release) with clock | Next generation smart speaker with powerful bass, LED display and Alexa (White)"
                        price={74.16}
                        image="https://m.media-amazon.com/images/I/61I1wntVL4L._SX679_.jpg"
                        rating={3}
                    />
                    <Product
                        id='18364937'
                        title="OnePlus Nord CE 5G (Charcoal Ink, 8GB RAM, 128GB Storage)"
                        price={330.33}
                        image="https://m.media-amazon.com/images/I/71LRBr1aLNS._SX679_.jpg"
                        rating={4}
                    />
                </div>

                <div className="home__row">

                    <Product 
                        id='37352846'
                        title="AmazonBasics 139cm (55 inch) 4K Ultra HD Smart LED Fire TV AB55U20PS (Black)"
                        price={573.04}
                        image="https://m.media-amazon.com/images/I/71AqQyCMmeL._SX679_.jpg"
                        rating={4}
                    />
                </div>
                <div className="home__minicartItems">
                <Card style={{width: '18rem'}}>
                    <ListGroup variant='flush'>
                        <ListGroup.Item>
                            {basket.map(item => (
                                <MiniCartItems
                                    key={item.id}  
                                    id={item.id}                         
                                    title={item.title}
                                />
                            ))}
                        </ListGroup.Item>
                    </ListGroup>
                </Card>
            </div>
            </div>
        </div>
    )
}

export default Home;
