import React from 'react';
import './Login.css';
import { Link, useHistory } from 'react-router-dom';
import { useState } from 'react';
import { auth } from '../../firebase';
import { userInfo } from 'os';

function Login() {
    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const signIn = e => {
        e.preventDefault();
        //firebase login 
        auth
            .signInWithEmailAndPassword(email, password)
            .then(auth => {
                history.push('/')
            })
            .catch(error => alert(error.message));
    };

    const register = e => {
        e.preventDefault();
        //firebase register
        auth
            .createUserWithEmailAndPassword(email, password)
            .then((auth) => {
                //it successfully created an user with email and password
                if(auth) {
                    history.push('/')
                }
            })
            .catch(error => alert(error.message));
    };

    return (
        <div className="login">
            <Link to="/">
                <img 
                    className="login__logo"
                    src="https://upload.wikimedia.org/wikipedia/commons/a/a9/Amazon_logo.svg" 
                />
            </Link>

            <div className="login__container">
                <h1>Sign in</h1>
                <form>
                    <h5>E-mail</h5>
                    <input type='text' value={email} onChange={e => setEmail(e.target.value)} />

                    <h5>Password</h5>
                    <input type='password' value={password} onChange={e => setPassword(e.target.value)} />

                    <button className="login__signInButton" onClick={signIn} type='submit'>Sign In</button>
                </form>
                <p>By continuing, you agree to Amazon's Clone App Conditions of Use and Privacy Notice.</p>

                <button className="login__registerButton" onClick={register}>Create your Amazon Account</button>
            </div>
        </div>
    )
}

export default Login;
