import React, { useEffect } from 'react';
import './App.css';
//Import Components
import Header from './components/Header';
import Home from './components/Home';
import Checkout from './components/Checkout';
import Login from './components/Login';
import Payment from './components/Payment';
import Orders from './components/Orders';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { auth } from './firebase';
import { useStateValue } from './StateProvider';
import { loadStripe } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';

const promise = loadStripe('pk_test_51Jhcv2SAcSM7jFoUVhNin1hHg5n6PPYQpwpbsup6BpY4OljUuKx0G7xcLdN4Hpm76miGhnpMUtxU4PYz5LSormsz00x0krInfq');

function App() {
  const [{basket}, dispatch] = useStateValue();

  useEffect(() => {
    auth.onAuthStateChanged(authUser => {
      console.log('The user is >>>', authUser);
      
      if(authUser){
        //user logged in
        dispatch ({
          type: 'SET_USER',
          user: authUser
        })
      } else {
        //the user logged out
        dispatch ({
          type: 'SET_USER',
          user: null
        })
      }

    })
  }, []);

  return (
    //BEM
    <Router>
      <div className="app">
        <Switch>
          <Route path='/orders'>
            <Header />
            <Orders />
          </Route>
          <Route path='/login'>
            <Login />
          </Route>
          <Route path='/checkout'>
            <Header />
            <Checkout />
          </Route>
          <Route path='/payment'>
            <Header />
            <Elements stripe={promise}>
              <Payment />
            </Elements> 
          </Route>
          <Route path='/'>
            <Header />
            <Home />
          </Route>
        </Switch>
        
      </div>
    </Router>
  );
}

export default App;
